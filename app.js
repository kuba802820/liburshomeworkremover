const homeworkList = document.querySelectorAll(".myHomeworkTable > tbody > tr");
const tool = document.querySelector(
  "#body > div > div > table > thead > tr > td.big"
);

class Homework {
  static hideHomeworks = () => {
      App.renderButtons();
      chrome.storage.local.get((persistedData) => {
        if (!persistedData.showState) {
          return;
        };
        const deletedHomeworks = Object.values(persistedData);
        homeworkList.forEach((item) => {
          const deletedHomeworkID = item.getAttribute("id");
          deletedHomeworks.forEach((homework) => {
            if (homework.homeworkId === deletedHomeworkID) {
              item.setAttribute("style", "display: none");
            }
          });
        });
        Notiflix.Notify.Success("Pomyślnie ukryto usunięte zadania.");
      });
    };

  static showAllHomeworks = () => {
    App.renderButtons();
    Notiflix.Notify.Success("Pomyślnie pokazano usunięte zadania.");
    homeworkList.forEach((item) => {
      item.style.display = 'table-row';
    });
  };
  static hideOneHomework = ({
    item,
    teacher,
    schoolsubject,
    name,
    date,
    homeworkId,
  }) => {
    item.style.display = 'none';
    const obj = {};
    obj[`${homeworkId}`] = {
      teacher,
      schoolsubject,
      name,
      date,
      homeworkId,
      isShow: false,
    };
    chrome.storage.local.set(obj);
  };
   static showOneHomework = (homeworkId) => {
      chrome.storage.local.get((persistedData) => {
        const deletedHomeworks = Object.values(persistedData);
            Notiflix.Confirm.Show(
            "Na pewno?",
            "Czy chcesz przywrócić usunięte zadanie?",
            "Tak",
            "Nie",
            () => {
              const removedObject = deletedHomeworks.find(
                (item) => item.homeworkId === homeworkId
              );
              chrome.storage.local.remove([`${removedObject.homeworkId}`]);
              this.hideHomeworks();
              Notiflix.Notify.Success(`Pomyślnie przywrócono zadanie z ${removedObject.schoolsubject}`);
            },
            () => false
          );
      })
  }
}

class Button {
  static getButtonsAtrribs = ({
    title,
    isDisabled,
    homeworkManageBtn
  }) => {
    return {
      type: 'BUTTON',
      value: title,
      disabled: isDisabled,
      id:  homeworkManageBtn ? "homeworkaction_btn" : "",
      style: "z-index: 0;",
      className: `small ui-button ui-widget ui-state-default ui-corner-all ${isDisabled &&
      "ui-button-disabled ui-state-disabled"}`
    }
  }
  static createCustomButton = (
    title,
    parent,
    addSpaceBelow = false,
    func,
    homeworkManageBtn = false,
    isDisabled = false
  ) => {
    const button = document.createElement("input");

    const btnAtribs = Button.getButtonsAtrribs({
      title,
      isDisabled,
      homeworkManageBtn
    });

    for(let key in btnAtribs) {
      button[key] = btnAtribs[key];
    }
    
    if (addSpaceBelow) {
      const br = document.createElement("br");
      parent.append(br);
    }
    button.addEventListener("mouseenter", (e) => {
      button.classList.add("ui-state-hover");
    });
    button.addEventListener("mouseleave", (e) => {
      button.classList.remove("ui-state-hover");
    });
    button.addEventListener("click", func);
    parent.append(button);
  };
}

class App {
  static removeHomeworkManagedBtn = () => {
    document.querySelectorAll('#homeworkaction_btn').forEach((el) => el.remove())
  }
  static renderButtons = () => {
    this.removeHomeworkManagedBtn();
    chrome.storage.local.get((persistedData) => {
      const deletedHomeworks = Object.values(persistedData);
      homeworkList.forEach((item) => {
        const tds = item.querySelectorAll("td");
        const schoolsubject = tds[0].innerText;
        const teacher = tds[1].innerText;
        const name = tds[2].innerText;
        const date = tds[4].innerText;
        const actionSection = tds[9];
        const homeworkId = item.getAttribute("id");
        const isAlreadyRemoved = !!deletedHomeworks.find(
          (item) => item.homeworkId === homeworkId
        );
        Button.createCustomButton(
            "Przywróć ⟲",
            actionSection,
            false,
            () => {
              Homework.showOneHomework(homeworkId);
            },
            true,
            !isAlreadyRemoved
          );
        Button.createCustomButton(
            "Usuń ✘",
            actionSection,
            false,
            () => {
              Homework.hideOneHomework({
                item,
                teacher,
                schoolsubject,
                name,
                date,
                homeworkId,
              });
            },
            true,
            isAlreadyRemoved
          );
      });
    });
  }
  
}
(() => {
  Homework.hideHomeworks();
  Button.createCustomButton("Pokaż/Ukryj", tool, true, () => {
    chrome.storage.local.get(({ showState }) => {
      chrome.storage.local.set({ showState: !showState });
      if (showState) Homework.showAllHomeworks();
      else Homework.hideHomeworks();
    });
  });

  Button.createCustomButton("Statystk.", tool, true, () => {
    chrome.storage.local.get((persistedData) => {
      const storedDeletions = Object.values(persistedData);
      const temp = [];
      let formatedStats = "";
      storedDeletions.forEach((el) => {
        if (typeof el === "object") {
          temp.push(el.schoolsubject);
        }
      });
      const countedDeletedSubjects = temp.reduce(
        (a, b) => a.set(b, a.get(b) + 1 || 1),
        new Map()
      );
      for (const subject of countedDeletedSubjects) {
        const [name, deletedCount] = subject;
        formatedStats += `Zadań z ${name} usunąłeś: <b>${deletedCount}</b><br>`;
      }
      Notiflix.Report.Info("Statystyki", formatedStats, "Ok");
    });
  });
  Button.createCustomButton("Reset.", tool, true, () => {
    Notiflix.Confirm.Show(
      "Na pewno?",
      "Czy chcesz przywrócić usunięte zadania?",
      "Tak",
      "Nie",
      () => {
        chrome.storage.local.clear(() => {
          Notiflix.Report.Success(
            "Sukces",
            "Pomyślnie przywrócono wszystkie zadania!",
            "Ok"
          );
          Button.showAllHomeworks();
        });
      },
      () => false
    );
  });
  window.addEventListener("load", () => {
    Notiflix.Report.Init({
      width: "520px",
      plainText: false,
      messageMaxLength: 10000,
      info: {
        backOverlayColor: "rgba(0,0,0,0.5)",
      },
    });
  });
})();
